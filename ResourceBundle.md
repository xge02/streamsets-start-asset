

# ResourceBundle





Resource bundles contain locale-specific objects. When your program needs a locale-specific resource, a String for example, your program can load it from the resource bundle that is appropriate for the current user's locale. In this way, you can write program code that is largely independent of the user's locale isolating most, if not all, of the locale-specific information in resource bundles.

资源包包含特定于语言环境的对象。当您的程序需要特定于语言环境的资源(例如字符串)时，您的程序可以从适合当前用户的语言环境的资源包中加载它。通过这种方式，您可以编写在很大程度上独立于用户区域设置的程序代码，将大部分(如果不是全部)特定于区域设置的信息隔离在资源包中。

This allows you to write programs that can:

- be easily localized, or translated, into different languages  易于本地化，或翻译成不同的语言

- handle multiple locales at once  一次处理多个地区

- be easily modified later to support even more locales 可以轻松修改以支持更多地区



Resource bundles belong to families whose members share a common base name, but whose names also have additional components that identify their locales. For example, the base name of a family of resource bundles might be "MyResources". The family should have a default resource bundle which simply has the same name as its family - "MyResources" - and will be used as the bundle of last resort if a specific locale is not supported. The family can then provide as many locale-specific members as needed, for example a German one named "MyResources_de".

资源包属于这样的家庭，其成员共享一个公共的基名称，但其名称还包含标识其地区的其他组件。例如，一组资源包的基名称可能是“MyResources”。这个家族应该有一个默认的资源包，它的名称与它的家族相同——“MyResources”，如果不支持特定的语言环境，它将作为最后的选择。然后，家庭可以根据需要提供尽可能多的特定于语言环境的成员，例如一个名为“MyResources_de”的德语成员。



Each resource bundle in a family contains the same items, but the items have been translated for the locale represented by that resource bundle. For example, both "MyResources" and "MyResources_de" may have a String that's used on a button for canceling operations. In "MyResources" the String may contain "Cancel" and in "MyResources_de" it may contain "Abbrechen".

家庭中的每个资源包都包含相同的项，但这些项已被翻译为该资源包所表示的语言环境。例如，“MyResources”和“MyResources_de”可能都有一个用于取消操作的按钮的字符串。在“MyResources”中，字符串可能包含“Cancel”，在“MyResources_de”中，它可能包含“Abbrechen”。



If there are different resources for different countries, you can make specializations: for example, "MyResources_de_CH" contains objects for the German language (de) in Switzerland (CH). If you want to only modify some of the resources in the specialization, you can do so.

如果不同的国家有不同的资源，您可以进行专门化:例如，“MyResources_de_CH”包含瑞士的德语(de)对象(CH)。如果您只想修改专门化中的某些资源，您可以这样做。



When your program needs a locale-specific object, it loads the ResourceBundle class using the getBundle method:

当你的程序需要一个特定于语言环境的对象时，它会使用getBundle方法加载ResourceBundle类:

```java
ResourceBundle myResources =
        ResourceBundle.getBundle("MyResources", currentLocale);

Resource bundles contain key/value pairs. The keys uniquely identify a locale-specific object in the bundle. Here's an example of a ListResourceBundle that contains two key/value pairs:
   public class MyResources extends ListResourceBundle {
       protected Object[][] getContents() {
           return new Object[][] {
               // LOCALIZE THE SECOND STRING OF EACH ARRAY (e.g., "OK")
               {"OkKey", "OK"},
               {"CancelKey", "Cancel"},
               // END OF MATERIAL TO LOCALIZE
          };
       }
   }
```





Keys are always Strings. In this example, the keys are "OkKey" and "CancelKey". In the above example, the values are also Strings--"OK" and "Cancel"--but they don't have to be. The values can be any type of object.
You retrieve an object from resource bundle using the appropriate getter method. Because "OkKey" and "CancelKey" are both strings, you would use getString to retrieve them:

键总是字符串。在本例中，键是“OkKey”和“CancelKey”。在上面的例子中，值也是字符串——“OK”和“Cancel”——但它们不必是字符串。值可以是任何类型的对象。

使用适当的getter方法从资源包中检索对象。因为“OkKey”和“CancelKey”都是字符串，你可以使用getString来检索它们:

```java
   button1 = new Button(myResources.getString("OkKey"));
   button2 = new Button(myResources.getString("CancelKey"));
```

The getter methods all require the key as an argument and return the object if found. If the object is not found, the getter method throws a MissingResourceException.
Besides getString, ResourceBundle also provides a method for getting string arrays, getStringArray, as well as a generic getObject method for any other type of object. When using getObject, you'll have to cast the result to the appropriate type. For example:

getter方法都需要键作为参数，如果找到，则返回对象。如果没有找到对象，getter方法将抛出一个MissingResourceException异常。

除了getString之外，ResourceBundle还提供了获取字符串数组的方法getStringArray，以及用于任何其他类型对象的通用getObject方法。在使用getObject时，您必须将结果转换为适当的类型。例如:

```java
   int[] myIntegers = (int[]) myResources.getObject("intList");
```

The Java Platform provides two subclasses of ResourceBundle, ListResourceBundle and PropertyResourceBundle, that provide a fairly simple way to create resources. As you saw briefly in a previous example, ListResourceBundle manages its resource as a list of key/value pairs. PropertyResourceBundle uses a properties file to manage its resources.

Java平台提供了ResourceBundle的两个子类，ListResourceBundle和PropertyResourceBundle，它们提供了一种相当简单的创建资源的方法。正如在前面的示例中简要看到的，ListResourceBundle将其资源管理为键/值对列表。PropertyResourceBundle使用一个属性文件来管理它的资源。

If ListResourceBundle or PropertyResourceBundle do not suit your needs, you can write your own ResourceBundle subclass. Your subclasses must override two methods: handleGetObject and getKeys().
The implementation of a ResourceBundle subclass must be thread-safe if it's simultaneously used by multiple threads. The default implementations of the non-abstract methods in this class, and the methods in the direct known concrete subclasses ListResourceBundle and PropertyResourceBundle are thread-safe.

如果ListResourceBundle或PropertyResourceBundle不适合您的需要，您可以编写自己的ResourceBundle子类。您的子类必须覆盖两个方法:handleGetObject和getKeys()。

ResourceBundle子类的实现必须是线程安全的，如果它同时被多个线程使用。该类中非抽象方法的默认实现，以及直接已知的具体子类ListResourceBundle和PropertyResourceBundle中的方法是线程安全的。



**ResourceBundle.Control**

The ResourceBundle.Control class provides information necessary to perform the bundle loading process by the getBundle factory methods that take a ResourceBundle.Control instance. You can implement your own subclass in order to enable non-standard resource bundle formats, change the search strategy, or define caching parameters. Refer to the descriptions of the class and the getBundle factory method for details.

ResourceBundle。控件类提供了通过获取ResourceBundle的getBundle工厂方法执行bundle加载过程所需的信息。控制实例。您可以实现自己的子类来启用非标准资源包格式、更改搜索策略或定义缓存参数。有关详细信息，请参阅类和getBundle工厂方法的描述。

For the getBundle factory methods that take no ResourceBundle.Control instance, their default behavior of resource bundle loading can be modified with installed ResourceBundleControlProvider implementations. Any installed providers are detected at the ResourceBundle class loading time. If any of the providers provides a ResourceBundle.Control for the given base name, that ResourceBundle.Control will be used instead of the default ResourceBundle.Control. If there is more than one service provider installed for supporting the same base name, the first one returned from ServiceLoader will be used.

对于不接受ResourceBundle的getBundle工厂方法。控件实例，可以使用已安装的ResourceBundleControlProvider实现修改其资源包加载的默认行为。在ResourceBundle类装入时检测到任何已安装的提供程序。如果任何提供者提供了ResourceBundle。控件，用于给定的基本名称ResourceBundle。控件将代替默认的ResourceBundle.Control。如果安装了多个服务提供者来支持相同的基本名称，则将使用ServiceLoader返回的第一个服务提供者。

**Cache Management**
Resource bundle instances created by the getBundle factory methods are cached by default, and the factory methods return the same resource bundle instance multiple times if it has been cached. getBundle clients may clear the cache, manage the lifetime of cached resource bundle instances using time-to-live values, or specify not to cache resource bundle instances. Refer to the descriptions of the getBundle factory method, clearCache, ResourceBundle.Control.getTimeToLive, and ResourceBundle.Control.needsReload for details.

默认情况下，由getBundle工厂方法创建的资源包实例将被缓存，如果资源包实例已被缓存，则工厂方法将多次返回该资源包实例。getBundle客户机可以清除缓存，使用生存时间值管理缓存的资源包实例的生存期，或者指定不缓存资源包实例。请参考getBundle工厂方法、clearCache、resourcebundl . control的描述。getTimeToLive, ResourceBundle.Control。needsReload详情。

**Example**
The following is a very simple example of a ResourceBundle subclass, MyResources, that manages two resources (for a larger number of resources you would probably use a Map). Notice that you don't need to supply a value if a "parent-level" ResourceBundle handles the same key with the same value (as for the okKey below).

下面是ResourceBundle子类MyResources的一个非常简单的示例，它管理两个资源(对于更多的资源，您可能会使用映射)。请注意，如果“父级”ResourceBundle处理具有相同值的相同键(对于下面的okKey)，则不需要提供值。

```java
// default (English language, United States)
public class MyResources extends ResourceBundle {
    public Object handleGetObject(String key) {
        if (key.equals("okKey")) return "Ok";
        if (key.equals("cancelKey")) return "Cancel";
        return null;
    }
    public Enumeration<String> getKeys() {
        return Collections.enumeration(keySet());
    }

    // Overrides handleKeySet() so that the getKeys() implementation
    // can rely on the keySet() value.
    protected Set<String> handleKeySet() {
        return new HashSet<String>(Arrays.asList("okKey", "cancelKey"));
    }
}
   
// German language
public class MyResources_de extends MyResources {
    public Object handleGetObject(String key) {
        // don't need okKey, since parent level handles it.
        if (key.equals("cancelKey")) return "Abbrechen";
        return null;
    }
    protected Set<String> handleKeySet() {
        return new HashSet<String>(Arrays.asList("cancelKey"));
    }
}
```



You do not have to restrict yourself to using a single family of ResourceBundles. For example, you could have a set of bundles for exception messages, ExceptionResources (ExceptionResources_fr, ExceptionResources_de, ...), and one for widgets, WidgetResource (WidgetResources_fr, WidgetResources_de, ...); breaking up the resources however you like.



您不必局限于使用单一的resourcebundle家族。例如，可以有一组用于异常消息的包，ExceptionResources (ExceptionResources_fr, ExceptionResources_de，…)和一个用于小部件的包，WidgetResource (WidgetResources_fr, WidgetResources_de，…);按你喜欢的方式分割资源。



Since:
JDK1.1
See Also:
ListResourceBundle, PropertyResourceBundle, MissingResourceException
  < 1.8 >